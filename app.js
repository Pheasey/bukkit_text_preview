function loadInput() {
  if (localStorage.input) {
    document.querySelector('#input').value = localStorage.input
  }
  makeOutput()
}

window.addEventListener('load', function() {
  loadInput()
})


function parse(input) {
  return ('&r' + input)
  // ensure text begins with default color
  .match(/([&§][0-9a-fk-or](?:(?![&§][0-9a-fk-or])[\s\S])*)/ig)
  .map(function(x) {
    var code = x.charAt(1).toLowerCase()
    return {
      code: code,
      text: x.substring(2)
    }
  })
  // [{code: "b", text: "hello"}, {code: "o", text: "world"}, {code: "r", text: "!"}, ...]
}

function stack(parsed) {
  function classify(codes) {
    return codes.reduce(function(p, c) {
      return p + ' mc-' + c
    }, '').trim()
    // make array of codes into string of classes
  }
  var cache = []
  return parsed.map(function(x) {
    if (/[0-9a-fr]/i.test(x.code)) {
      // first level codes (colors)
      cache = [x.code]
      // reset cache with this first level code
      return {
        text: x.text,
        classes: classify(cache)
      }
    } else {
      // second level codes (formatting)
      cache.push(x.code)
      // stack this second level code into cache
      return {
        text: x.text,
        classes: classify(cache)
      }
    }
  })
  // [{code: "b", text: "hello"}, {classes: "mc-b mc-o", text: "world"}, {classes: "mc-r", text: "!"}, ...]
}

function render(stacked) {
  var elm = d3.select('#output')
    .selectAll('span')
    .data(stacked)
      .text(function(d) { return d.text })
      .attr('class', function(d) { return d.classes })

  elm.exit().remove()

  elm.enter().append('span')
    .text(function(d) { return d.text })
    .attr('class', function(d) { return d.classes })
}

function makeOutput() {
  render(
    stack(
      parse(
        document.querySelector('#input').value
        // "&bhello &oworld&r!"
      )
    )
  )
}

document.querySelector('#input').addEventListener('input', function(event) {
  localStorage.input = event.target.value
  // save input
  makeOutput()
})

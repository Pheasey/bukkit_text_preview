## [mctextbin.gitlab.io](https://mctextbin.gitlab.io) | mctextbin | live preview editor for minecraft text formatting

This is a text editor that lets you type text on the left with minecraft formatting codes, and see the result on the right as it will appear in game!

For example:
> &bhello &oworld&r!

Turns into:
> ![](https://gitlab.com/mctextbin/mctextbin.gitlab.io/uploads/dabaace26e4ea9dd3a3b434a248da331/output.png)

More help for how to use formatting codes: http://minecraft.gamepedia.com/Formatting_codes

This is the legacy format (the new format is JSON). It's still used in many places such as server config files, world names (with an NBT editor), books, and server chat/nicknames on some servers.

---

## Made by [Jonathan Herman](https://lasercar.gitlab.io). I'd love to hear any feedback! :)
![](http://forthebadge.com/images/badges/built-with-love.svg)

- [Tweet](https://twitter.com/lasercar/status/773730888165175296)
- [Hacker News thread](https://news.ycombinator.com/item?id=12450218)
- [/r/Minecraft thread](https://redd.it/51pge6)
- [Issue tracker](https://gitlab.com/mctextbin/mctextbin.gitlab.io/issues)

---

Copyright 2016 Jonathan Herman. MIT License.